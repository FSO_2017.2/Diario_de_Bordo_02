#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>

#define PHILOSOPHES 5   	               /* número de filósofos na mesa */
#define HUNGRY 0                    	   /* diretiva que indica se está com fome*/
#define EATING 1 	                	     /* diretiva que indica se está comendo */
#define THINKING 2	                	   /* diretiva que indica se está filosofando */

int state[PHILOSOPHES];                 /* Estado de cada filósofo*/
int philosophes_time = 1, eat_time = 1; /* tempo para filosofar e para comer */

sem_t *mutex;                           /* exclusão mutua da área crítica  */
sem_t *eated[PHILOSOPHES];              /* variáveis que armazenarão a quantidade de vezes que comeu */
pthread_t *philosophes[PHILOSOPHES];    /* variáveis que conterão as threads */

void initialize();
void try_change_to_eat(int index);
void *philosopher(void *index);

int main() {
  int i;
  int *index;

  initialize();
  
  /* Criação dos filósofos segundo a quantidade máxima */
  for (i = 0; i < PHILOSOPHES; i++) {
    index = malloc(sizeof(int));
    *index = i;
    pthread_create(philosophes[i], NULL, philosopher, index);
  }
  
  while(1){
  	int count = 0;
	int i;
	for(i = 0; i < PHILOSOPHES; i++){
		int aux;
		sem_getvalue(eated[i], &aux);
		if(aux){
			count++;
		}
	}
 	if(count == PHILOSOPHES){
		printf("Todos os filósofos ja comeram ao menos 1 vez\n");
		break;
	} 
  }

  /* Garantir que o processo pai só encerre quando todas as threads terminarem */
  for (i = 0; i < PHILOSOPHES; i++) {
    pthread_join(*philosophes[i], NULL);
  }

  return 0;
}

/* Função responsável pela inicialização dos filósofos
 * e semáforos com seus valores indicando que os talheres
 * estão livres e o semáforo da região crítica.
 * */
void initialize()
{
  int i;
  for (i = 0; i < PHILOSOPHES; i++) {
    philosophes[i] = malloc(sizeof(pthread_t));
	eated[i] = malloc(sizeof(sem_t));
	sem_init(eated[i], 0, 0);
  }
  mutex = malloc(sizeof(sem_t));
  sem_init(mutex, 0, 1);

}

/* Função que verifica a disponibilidade dos talheres e
 * tenta mudar o status do filósofo para comendo
 * @param index recebe o id do filósofo que está tentando mudar seu status
 * */
void try_change_to_eat(int index)
{
  if(state[index] == HUNGRY &&
     state[ (index + PHILOSOPHES - 1) % PHILOSOPHES ] != EATING &&
     state[ (index + 1) % PHILOSOPHES ] != EATING)
  {
    printf("Eu, o filósofo %d estou comendo\n", index);
    state[index] = EATING;
	sem_post(eated[index]);
  }
}

/* Função responsável pela atividade de cada filósofo:
 * Fica com fome; Tenta pegar os talhares; se conseguir,
 * come, caso contrário, continua a execução e vai pensar.
 * @param index recebe o id do filósofo
 * */
void *philosopher(void *index)
{
  int my_index = *((int *)index);
  free(index);
  while (1) {

    sem_wait(mutex);
    state[my_index] = HUNGRY;
    printf("Eu, o filósofo %d estou com fome\n", my_index);
    
    /* Tenta pegar os talheres */
    try_change_to_eat(my_index);
    sem_post(mutex);

    /* Tempo para comer */
    sleep(eat_time);

    /* Devolve talheres */
    sem_wait(mutex);
    state[my_index] = THINKING;
    printf("Eu, o filósofo %d estou Pensando\n", my_index);
    sleep(philosophes_time);
    try_change_to_eat((my_index + 1) % PHILOSOPHES);
    try_change_to_eat((my_index + PHILOSOPHES - 1) % PHILOSOPHES);
    sem_post(mutex);
  }
}
