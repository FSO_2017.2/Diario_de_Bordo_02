#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>

#define CHAIRS 5		/* diretiva que define a quantidade de cadeiras para fila de espera */

sem_t *customers;		/* numero de clientes à espera de atendimento */
sem_t *barbers;			/* numero de barbeiros à espera de clientes */
sem_t *mutex;			/* exclusão mutua da região critica */
int waiting = 0;		/* fila de espera */
int time_of_cut;		/* tempo que o barbeiro leva para cortar os cabelos */
int time_of_new_client;	/*tempo para novo cliente chegar */


void initialize(char x, char y);
void *barber_function();
void *costumer_function(void *password);

/* Existe uma limatação nos argumentos. Os dois valores
 * devem estar no intervalo 0 e 9
 * */
int main(int argc, char const *argv[])
{
  pthread_t *costumer = (pthread_t*) malloc(sizeof(pthread_t));
  pthread_t *barber = (pthread_t*) malloc(sizeof(pthread_t));
  int *password = (int*)malloc(sizeof(int));
  int i;

  initialize(argv[1][0] - '0', argv[2][0] - '0');

  /* Criação de 1 barbeiro */
  pthread_create(barber, NULL, barber_function, NULL);

  
  /* Criação de infinitos clientes */
  for (i = 1; 1; i++)
  {
    password = (int*)malloc(sizeof(int));
    *password = i;
    pthread_create(costumer, NULL, costumer_function, password);
    sleep(time_of_new_client);
  }
  return 0;
}

/* Função responsável pela inicialização dos semáforos
 * com seus valores iniciais de clientes, barbeiro e cadeira
 * e tempo de execução das atividades.
 * */
void initialize(char x, char y){
  customers = (sem_t*) malloc(sizeof(sem_t));
  barbers = (sem_t*) malloc(sizeof(sem_t));
  mutex = (sem_t*) malloc(sizeof(sem_t));

  sem_init(customers, 0, 0);
  sem_init(barbers, 0, 0);
  sem_init(mutex, 0, 1);

  time_of_cut = x;
  time_of_new_client = y;
}

/* Função responsável pela atividade do barbeiro: 
 * Verifica se possui clientes para atender.
 * */
void *barber_function()
{
  while(1)
  {
    int *x = malloc(sizeof(int));
    sem_getvalue(customers, x);

/* dorme até aparecer novo cliente. */
    if(!(*x))
      printf("dormindo\n");
    sem_wait(customers);

/* retira cliente da fila de espera,
 * abrindo uma nova vaga, e começa a cortar seu cabelo. 
 */
	sem_wait(mutex);
    waiting--;
    sem_post(barbers);
    sem_post(mutex);
    printf("Barbeiro trabalhando!\n");
    sleep(time_of_cut);
  }
  return NULL;
}

/* Função responsável pela atividade do cliente: 
 * Cliente chega na barbearia e verifica se existe
 * espaço na fila de espera.
 * @param password recebe a senha de chegada no barbeiro
 * */
void *costumer_function(void *password)
{
  sem_wait(mutex);
  printf("Mais um cliente chegou - Senha:%d\n", *((int *)password));
  if(waiting < CHAIRS){
  /* ocupa um espaço da fila, acorda o barbeiro ou
   * espera na fila até ser atendido pelo barbeiro
   * por ordem de chegada.
   */
    waiting++;
    sem_post(mutex);
    sem_post(customers);
    sem_wait(barbers);
    printf("Cortando Cabelo do cliente! - Senha:%d\n", *((int *)password));
  }
  else
  {
    /* Cliente desiste de ter o cabelo cortado */
    sem_post(mutex);
    printf("Cliente foi embora - Senha:%d\n", *((int *)password));
  }
  
  free(password);
  pthread_exit(0);
  return NULL;
}
